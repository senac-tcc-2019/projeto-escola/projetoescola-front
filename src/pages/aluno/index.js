import React, { Component } from 'react';
import api from '../../services/api';
import '../../pages/aluno/styles.css';


export default class Aluno extends Component {
    state = {
        alunos: [],
    }


    componentDidMount() {
        this.loadAlunos();
    }
    loadAlunos = async () => {
        const response = await api.get('/aluno');

        this.setState({ alunos: response.data });
    }
    render() {
        return (

            <section id="aluno-list">
                <article>
                    <boddy>
                        <button className="novo">Novo </button>
                        <div className="table">
                            <table>
                                <tr>
                                    <th>ID:</th>
                                    <th>Nome</th>
                                    <th>Cpf</th>
                                    <th>Data Nascimento</th>
                                    <th>Ativo</th>
                                    <th>Ações</th>
                                </tr></table>
                        </div>
                    </boddy>
                    <div className="aluno-list">
                        <table>
                            {this.state.alunos.map(aluno=> (<tr><th>{aluno.id}</th><th>{aluno.nome}</th><th>{aluno.cpf}</th><th>{aluno.data_nascimento}</th><th>{aluno.ativo}</th><th><button className="edit">Editar</button><button className="delete">Excluir</button></th></tr>))}
                        </table> </div>

                </article>

            </section>
        );
    }

}