import React, { Component } from 'react';
import api from '../../services/api';
import '../../pages/diciplina/styles.css';

export default class Diciplina extends Component {
    state = {
        diciplinas: [],
    }


    componentDidMount() {
        this.loadDiciplinas();
    }
    loadDiciplinas = async () => {
        const response = await api.get('/diciplina');

        this.setState({ diciplinas: response.data });
    }
    render() {
        return (

            <section id="diciplina-list">
                <article>
                    <boddy>
                        <button className="novo">Novo</button>
                        <div className="table">
                            <table>
                                <tr>
                                    <th>ID:</th>
                                    <th>Nome</th>
                                    <th>Ativo</th>
                                    <th>Ações</th>
                                </tr></table>
                        </div>
                    </boddy>
                    <div className="diciplina-list">
                        <table>
                            {this.state.diciplinas.map(diciplina => (<tr><th>{diciplina.id}</th><th>{diciplina.nome}</th><th>{diciplina.ativo}</th><th><button className="edit">Editar</button><button className="delete">Excluir</button></th></tr>))}
                        </table> </div>

                </article>

            </section>
        );
    }
}