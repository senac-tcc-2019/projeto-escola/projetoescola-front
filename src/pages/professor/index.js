import React, { Component } from 'react';
import api from '../../services/api';

export default class Professor extends Component {
    state = {
        professores: [],
    }


    componentDidMount() {
        this.loadProfessores();
    }
    loadProfessores = async () => {
        const response = await api.get('/professor');

        this.setState({ professores: response.data });
    }
    render() {
        return (

            <section id="professor-list">
                <article>
                    <boddy>
                        <button className="novo">Novo</button>
                        <div className="table">
                            <table>
                                <tr>
                                    <th>ID:</th>
                                    <th>Nome</th>
                                    <th>Cpf</th>
                                    <th>Data Nascimento</th>
                                    <th>Ativo</th>
                                    <th>Ações</th>
                                </tr></table>
                        </div>
                    </boddy>
                    <div className="professor-list">
                        <table>
                            {this.state.professores.map(professor => (<tr><th>{professor.id}</th><th>{professor.nome}</th><th>{professor.cpf}</th><th>{professor.data_nascimento}</th><th>{professor.ativo}</th><th><button className="edit">Editar</button><button className="delete">Excluir</button></th></tr>))}
                        </table> </div>

                </article>

            </section>
        );
    }
}