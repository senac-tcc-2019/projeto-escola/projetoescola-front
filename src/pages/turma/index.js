import React, { Component } from 'react';
import api from "../../services/api";
import '../../pages/turma/styles.css';

export default class Turma extends Component {
    state = {
        turmas: [],
    }

    componentDidMount() {
        this.loadTurmas();
    }
    loadTurmas = async () => {
        const response = await api.get('/turma');

        this.setState({ turmas: response.data });
    }
    render() {
        return (
            <section id="turma-list">
                <article>
                    <boddy>
                        <div className="table">
                            <table>
                                <tr>
                                    <th>ID:</th>
                                    <th>Nome</th>
                                    <th>Numero de Aulas</th>
                                    <th>Data Inicial</th>
                                    <th>Ações</th>
                                </tr></table>
                        </div>
                    </boddy>
                    <div className="turma-list">
                        <table>
                        {this.state.turmas.map(turma => ( <tr><th>{turma.id}</th><th>{turma.nome}</th><th>{turma.n_aulas}</th><th>{turma.data_inicio}</th><th><button className="edit">Editar</button><button className="delete">Excluir</button></th></tr> ))}
                    </table> </div>

                </article>

            </section>
        );
    }
}