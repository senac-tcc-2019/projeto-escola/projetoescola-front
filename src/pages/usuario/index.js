import React, { Component } from 'react';
import api from "../../services/api";

export default class Usuario extends Component {
    state = {
        usuarios: [],
    }

    componentDidMount() {
        this.loadUsuarios();
    }
    loadUsuarios = async () => {
        const response = await api.get('/usuario');

        this.setState({ usuarios: response.data });
    }
    render() {
        return (

            <section id="usuario-list">
                <article>
                    <boddy>
                        <button className="novo">Novo</button>
                        <div className="table">
                            <table>
                                <tr>
                                    <th>ID</th>
                                    <th>Nome</th>
                                    <th>Ativo</th>
                                    <th>Ações</th>
                                </tr></table>
                        </div>
                    </boddy>
                    <div className="usuario-list">
                        <table>
                            {this.state.usuarios.map(usuario => (<tr><th>{usuario.id}</th><th>{usuario.nome_completo}</th><th>{usuario.ativo}</th><th><button className="edit">Editar</button><button className="delete">Excluir</button></th></tr>))}
                        </table> </div>

                </article>

            </section>
        );

    }
}
