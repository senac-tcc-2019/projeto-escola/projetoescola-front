import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import Main from './pages/main';
import Turma from './pages/turma';
import Usuario from './pages/usuario';
import Diciplina from'./pages/diciplina';
import Aluno from './pages/aluno';
import Professor from './pages/professor';


const Routes = ( ) =>  (
    <BrowserRouter>
    <Switch>
        <Route exact  path ="/" component={Main} />
        <Route exact  path="/turma" component={Turma} />
        <Route exact  path="/usuario" component={Usuario} />
        <Route exact  path ="/diciplina" component={Diciplina} />
        <Route exact path ="/aluno" component={Aluno} />
        <Route exact path ="/professor" component={Professor} />
    </Switch>
    </BrowserRouter>
);
export default Routes;
