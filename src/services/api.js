import axios from 'axios';

const api = axios.create({baseURL: "https://projetoescola-api.herokuapp.com"
});
export default api;